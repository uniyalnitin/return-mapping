# Generated by Django 2.1.3 on 2018-11-12 09:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ans', models.CharField(max_length=32)),
            ],
        ),
        migrations.CreateModel(
            name='QuestionTen',
            fields=[
                ('q_no', models.AutoField(primary_key=True, serialize=False)),
                ('question_type', models.CharField(choices=[('Y/N', 'Yes/No'), ('M/G', 'Megrate/Generate'), ('T', 'Text')], max_length=100)),
                ('question_text', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='QuestionType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question_type', models.CharField(max_length=32)),
            ],
        ),
        migrations.AddField(
            model_name='answer',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.QuestionTen'),
        ),
    ]
