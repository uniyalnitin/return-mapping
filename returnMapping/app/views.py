from django.shortcuts import render
from django.http import HttpResponse
from .forms import QuestionTenForm, AnswerForm

# Create your views here.
def index(request):
    form = AnswerForm()
    return render(request,'base.html', {'form': form})
