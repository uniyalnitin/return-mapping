from django.contrib import admin
from app.models import QuestionType, QuestionTen, Answer

# Register your models here.
@admin.register(QuestionType)
class QuestionTypeAdmin(admin.ModelAdmin):
    list_display= ('question_type',)

@admin.register(QuestionTen)
class QuestionTenAdmin(admin.ModelAdmin):
    list_display = ('q_no', 'question_type', 'question_text')

@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = ('question','ans', )
