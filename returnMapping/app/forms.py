from django.forms import ModelForm
from .models import QuestionType, QuestionTen, Answer

class QuestionTenForm(ModelForm):
    class Meta:
        model = QuestionTen
        fields = ['q_no', 'question_type', 'question_text']



class AnswerForm(ModelForm):
    class Meta:
        model = Answer
        fields = ['question', 'ans']
