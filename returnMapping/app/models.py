from django.db import models

# Create your models here.

class QuestionType(models.Model):
    question_type = models.CharField(max_length=32)

    def __str__(self):
        return self.question_type

CHOICES_QT = (
            ('Y/N','Yes/No'),
            ('M/G', 'Migrate/Generate'),
            ('T', 'Text')
            )

class QuestionTen(models.Model):
    q_no = models.AutoField(primary_key=True)
    # question_type = models.ForeignKey(QuestionType, on_delete = models.CASCADE)
    question_type = models.CharField(max_length=100, choices = CHOICES_QT)
    question_text = models.CharField(max_length = 100)

    def __str__(self):
        return self.question_text

class Answer(models.Model):
    question = models.ForeignKey(QuestionTen, on_delete=models.CASCADE)
    ans = models.CharField(max_length=32)

    # def determine_answer_type(self):
    #     if self.question.question_type == 'Y/N':
    #         ans = models.CharField(max_length=32, choices = CHOICES_QT)

    def __str__(self):
        return self.ans
